angular.module('admin.login', ['ng-admin'])

.config(function($stateProvider){
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.tpl.html',
      controller: 'LoginCtrl'
    });
})

.run(function(Restangular, $state){

	Restangular
	.setErrorInterceptor(function(response, deferred, responseHandler) {
			if(response.status === 401) {
					$state.go('login');

					return false; // error handled
			}

			return true; // error not handled
	});

})

.controller('LoginCtrl', function($scope, $http, $state){

	$scope.login = function(credentials){
		return $http.post('/auth/login', {
			email: credentials.username,
			password: credentials.password
		})
		.then(function(resp){
			if (resp.data.user === false){
				delete $scope.credentials.password;
				$scope.response = resp.data.message;
			} else {
				//TODO: configure headers with token
				$state.go('dashboard');
			}
		})
	}
})
