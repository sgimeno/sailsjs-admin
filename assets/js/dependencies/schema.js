function Schema(){

  var ajax = {
    load: function(config){
      var promise = new Promise(function(resolve, reject){
        // Instantiates the XMLHttpRequest
        var client = new XMLHttpRequest();

        client.open('GET', '/config/entities');
        client.send();

        client.onload = function () {
          if (this.status == 200) {
            // Performs the function "resolve" when this.status is equal to 200
            resolve(this.response);
          } else {
            // Performs the function "reject" when this.status is different than 200
            reject(this.statusText);
          }
        };
        client.onerror = function () {
          reject(this.statusText);
        };
      });

      return promise;
    }

  };


  return {
    'load': function(config){
      return ajax.load(config);
    }
  };
};
