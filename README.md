SailsJS auto-generated AdminUI
================================

a [Sails](http://sailsjs.org) application

###Dependencies

 + [bower](http://www.bower.io)
 + [Sails](http://sailsjs.org)


```
>npm install -g sails bower-cli
```


###Setup

```
>git clone git@bitbucket.org:sgimeno/sailsjs-admin.git
>npm install
>bower install # just if npm postinstall hook fails
>sails lift
```

###Development

```
>grunt watch ## to execute pipeline tasks on source changes
>nodemon app.js ## to reload the sails server
```


Open http://localhost:1337. (change the port by setting $PORT variable)
