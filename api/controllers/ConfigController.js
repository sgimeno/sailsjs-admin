/**
 * ConfigController
 *
 * @description :: Server-side logic for managing Configs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var User = require('../controllers/UserController');

module.exports = {
	entities: function(req, res){
		var entities = {
			user: User.attributes
		};

		res.status(200)
			.json({
				entities: entities,
				apiUrl: process.env.API_URL
			});
	}
};
