/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

function encrypt(user, cb){
  bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
          if (err) {
              console.log(err);
              cb(err);
          } else {
              user.password = hash;
              cb();
          }
      });
  });
}

module.exports = {

  attributes: {
    email: {
            type: 'email',
            required: true,
            unique: true
        },
        password: {
            type: 'string',
            minLength: 6,
            required: true
        },
        toJSON: function() {
            var obj = this.toObject();
            delete obj.password;
            return obj;
        }
  },
  beforeCreate: function(user, cb) {
    encrypt(user, cb);
  },
  beforeUpdate: function(user, cb) {
    if (user.password) encrypt(user, cb);
    else cb();
  }

};
